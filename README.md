## Prerequisite
 - `Node` installed version > 8
 - `npm` or `yarn` installed version > 5
## Setup
 -  Insall`npm install`
## Run
 -  `npm start` or `ng serve`

## Lint and Test
 - `npm run lint`
 - `npm run test`
## Development server
Run `ng serve` for running local. Navigate to `http://localhost:4200/`. if you have any new change with your the source files, this application will automatically update.
## Deployment
 - `npm run deploy:firebase`
## Program structure
1. `layout`: In Layout folder is root layout that use for all project;
2. `modules`: I create only one `transaction` module, In this module we have one `components` folder including `transaction-form` component and `transaction-history` component;
    - `transaction-form` component is aim to make transfer
    - `transaction-history` component is aim to show the history of transactions
3. `router`: there is only 1 screen so the router will navigate directly to the test module
4. `assets`: Assets folder contains:
    - images,
    - icons,
    - fonts
    - scss
 - Mock `transactions.json` is placed in this folder
5. `pipes`: The pipes folder includes one custom money formatter pipe. `money-pipe.ts`
5. `environments`: We have two invironments in this project:
    - environment
    - environment
6. `constants`
   - This folder contains any constants of projectproject
7. `models`
   - This folder contains transaction models
## Functionality
1. Make transactions
2. List transactions history
3. Search transactions
4. Filter transactions by: `date`, `beneficary`, `amount`

## None Functional requirement
1. Responsive layout
3. Remote repository: https://gitlab.com/toolaugh/backbase-assignment/-/tree/master
4. Deloy static hosting: https://io-test-66803.firebaseapp.com/
5. Test included

I am looking forward to your feedback about my assignment. I hope that my project meets your quality expectation. Please review & give me your advice. Thanks