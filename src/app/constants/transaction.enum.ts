export enum SortDirection {
  ASC = 'asc',
  DESC = 'desc',
  NONE = ''
}

export enum SortType {
  DATE = 'date',
  AMOUNT = 'amount',
  BENEFICIARY = 'beneficiary'
}
