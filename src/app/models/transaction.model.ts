export class TransactionResponse {
  data: Transaction[];
}
export class Transaction {
  categoryCode?: string;
  dates?: Date;
  transaction?: TransactionData;
  merchant?: Merchant;
}
export class Date {
  valueDate?: number | string;
}
export class TransactionData {
  amountCurrency?: AmountCurrency;
  type?: string;
  creditDebitIndicator?: string;
}
export class AmountCurrency {
  amount?: number | string;
  currencyCode?: string;
}
export class Merchant {
  name?: string;
  accountNumber?: string;
}
