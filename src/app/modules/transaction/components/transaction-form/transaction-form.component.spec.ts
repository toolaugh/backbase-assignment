import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TransactionFormComponent } from './transaction-form.component';


describe('MakeTransferComponent', () => {
  let component: TransactionFormComponent;
  let fixture: ComponentFixture<TransactionFormComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionFormComponent],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatIconModule,
        BrowserAnimationsModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css('form'));
    el = de.nativeElement;
  });

  it('Should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should call on submit', () => {
    component.onSubmit();
    expect(component.isValidate).toEqual(true);
  });

  it('Form submit valid value', async () => {
    component.fromAccount = { name: 'Free Checking', amount: 5000 };
    component.transferForm.patchValue({
      toAccount: 'Son Test',
      amount: 500
    });
    component.onSubmit();
    expect(component.isConfirm).toBeTruthy();
    expect(component.isValidate).toBeTruthy();
  });

  it('Form emptly should be invalid', async () => {
    component.transferForm.patchValue({
      fromAccount: '',
      toAccount: '',
      amount: ''
    });
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    expect(component.isConfirm).toBeFalsy();
  });

  it('Should validate fail case transfer over balance', () => {
    component.fromAccount = { name: 'Free Checking', amount: 500 };
    component.transferForm.patchValue({
      toAccount: 'Son Test',
      amount: 5000
    });
    component.handleTransferTransaction();
    expect(component.isConfirm).toBeFalsy();
  });

  it('Form value submitted should be invalid', async () => {
    component.fromAccount = { name: 'Back Base', amount: 5000 };
    component.transferForm.patchValue({
      fromAccount: 'Back base',
      toAccount: 'Son test',
      amount: 100
    });
    spyOn(component, 'onSubmit').and.callThrough();
    component.onSubmit();
    expect(component.isConfirm).toBeTruthy();
  });

});
