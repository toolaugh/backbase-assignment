import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Transaction } from '@models/transaction.model';
import { TransactionService } from '@services/transaction/transaction.service';
@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html'
})
export class TransactionFormComponent implements OnInit {

  transferForm: FormGroup;
  isConfirm = false;
  isValidate = false;
  fromAccount = { name: 'Free Checking', amount: 5824.76 };

  constructor(
    private fb: FormBuilder,
    private transactionService: TransactionService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.transferForm = this.fb.group({
      fromAccount: [`${this.fromAccount.name} - $${this.fromAccount.amount}`, Validators.required],
      toAccount: ['', Validators.required],
      amount: ['', [
        Validators.required,
        Validators.min(0)
      ]],
    });
  }

  onSubmit() {
    this.isValidate = true;
    if (this.transferForm.invalid) {
      return;
    }
    if (Number(this.fromAccount.amount) - Number(this.transferForm.value.amount) < 500) {
      this.transferForm.controls.amount.setErrors({ balance: true });
      return;
    }
    this.isConfirm = true;
  }

  handleTransferTransaction() {
    this.onTransfer({ tranferData: this.transferForm.value });
    this.fromAccount.amount = Number(this.fromAccount.amount) - Number(this.transferForm.value.amount);
    this.resetForm();
  }

  resetForm() {
    this.transferForm.reset();
    this.isConfirm = false;
    this.isValidate = false;
    this.transferForm.patchValue({ fromAccount: `${this.fromAccount.name} - $${this.fromAccount.amount}` });
  }

  onTransfer({ tranferData }) {
    const transaction: Transaction = {
      categoryCode: '#12a580',
      dates: {
        valueDate: new Date().getTime()
      },
      transaction: {
        amountCurrency: {
          amount: tranferData.amount,
          currencyCode: 'EUR'
        },
        type: 'Salaries',
        creditDebitIndicator: 'CRDT'
      },
      merchant: {
        name: tranferData.toAccount,
        accountNumber: 'SI64397745065188826'
      }
    };
    this.transactionService.addNewTransaction(transaction);
  }
}
