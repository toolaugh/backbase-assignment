import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MoneyFormat } from 'app/pipes/money/money.pipe';
import { SafeUrlPipe } from 'app/pipes/safe-url.pipe';
import { TransactionHistoryComponent } from './transaction-history.component';
import { SortDirection, SortType } from '@constants/transaction.enum';
import { TransactionService } from '@services/transaction/transaction.service';
import { Transaction } from '@models/transaction.model';
import { HttpClientModule } from '@angular/common/http';
import transactions from '@assets/transactions.json';

describe('RecentTransactionsComponent', () => {
  let component: TransactionHistoryComponent;
  let fixture: ComponentFixture<TransactionHistoryComponent>;
  let mockTransactionList: Array<Transaction>;
  let transactionService: TransactionService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TransactionHistoryComponent,
        MoneyFormat,
        SafeUrlPipe
      ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatIconModule,
        BrowserAnimationsModule,
        HttpClientModule
      ],
      providers: [
        MoneyFormat,
        SafeUrlPipe,
        TransactionService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    mockTransactionList = transactions.data;
    transactionService = TestBed.get(TransactionService);
  });

  it('Should create', () => {
    expect(component).toBeTruthy();
  });

  it('Clear search', () => {
    spyOn(component, 'onClearSearch').and.callThrough();
    component.onClearSearch();
    expect(component.filter.searchText).toEqual('');
  });

  it('Search by text and not found results', () => {
    spyOn(component, 'onSearch').and.callThrough();
    component.filter.searchText = 'abc';
    component.onSearch(component.filter.searchText);
    expect(component.transactionList.length).toEqual(0);
  });

  it('Search by text and found data', () => {
    spyOn(component, 'onSearch').and.callThrough();
    component.filter.searchText = 'Back';
    component.onSearch(component.filter.searchText);
    expect(component.transactionList.length).not.toEqual(mockTransactionList.length);
  });

  it('Sort date by DESC', () => {
    component.filter.sortBy = {
      date: SortDirection.ASC,
      beneficiary: SortDirection.NONE,
      amount: SortDirection.NONE
    };
    component.sortOrders(SortType.DATE);
    expect(component.filter.sortBy.date).toEqual(SortDirection.DESC);
  });

  it('None short by Date', () => {
    component.filter.sortBy = {
      date: SortDirection.DESC,
      beneficiary: SortDirection.NONE,
      amount: SortDirection.NONE
    };
    component.sortOrders(SortType.DATE);
    expect(component.filter.sortBy.date).toEqual(SortDirection.NONE);
  });

  it('Sort beneficiary', () => {
    component.filter.sortBy = {
      date: SortDirection.ASC,
      beneficiary: SortDirection.ASC,
      amount: SortDirection.NONE
    };
    component.sortOrders(SortType.BENEFICIARY);
    expect(component.filter.sortBy.date).toEqual(SortDirection.ASC);
    expect(component.filter.sortBy.beneficiary).toEqual(SortDirection.DESC);
  });

  it('Sort amount', () => {
    component.filter.sortBy = {
      date: SortDirection.ASC,
      beneficiary: SortDirection.ASC,
      amount: SortDirection.NONE
    };
    component.sortOrders(SortType.AMOUNT);
    expect(component.filter.sortBy.date).toEqual(SortDirection.ASC);
    expect(component.filter.sortBy.beneficiary).toEqual(SortDirection.ASC);
    expect(component.filter.sortBy.amount).toEqual(SortDirection.ASC);
  });

});
