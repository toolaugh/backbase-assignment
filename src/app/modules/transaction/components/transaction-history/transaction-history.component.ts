import { Component, OnChanges, OnInit } from '@angular/core';
import { TransactionService } from '@services/transaction/transaction.service';
import { Transaction } from 'app/models/transaction.model';
import { SortDirection, SortType } from '@constants/transaction.enum';
@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html'
})
export class TransactionHistoryComponent implements OnInit, OnChanges {
  constructor(private transactionService: TransactionService) { }

  transactionList: Array<Transaction>;
  sortDirection = SortDirection;
  sortType = SortType;
  filter = {
    searchText: '',
    sortBy: {
      date: SortDirection.ASC,
      beneficiary: SortDirection.NONE,
      amount: SortDirection.NONE
    }
  };

  ngOnInit() {
    this.fetchTransactionList();
  }

  ngOnChanges() {
    if (this.transactionList) {
      this.sortOrders(SortType.DATE);
    }
  }

  /** Init transaction data */
  fetchTransactionList() {
    this.transactionService.fetchTransactions().subscribe(() => {
      this.transactionList = this.transactionService.getDefaultTransactions();
    });
  }

  onSearch(inputValue: string) {
    this.transactionList = this.transactionService.searchTransaction(inputValue);
  }

  onClearSearch() {
    this.transactionList = this.transactionService.getDefaultTransactions();
    this.filter.searchText = '';
  }

  sortOrders(property: string) {
    const type = this.filter.sortBy[property];
    if (type === SortDirection.ASC) {
      this.filter.sortBy[property] = SortDirection.DESC;
    } else if (type === SortDirection.DESC) {
      this.filter.sortBy[property] = SortDirection.NONE;
    } else {
      this.filter.sortBy[property] = SortDirection.ASC;
    }

    if (!this.filter.sortBy[property]) {
      return;
    }
    this.transactionList = this.transactionService.filterSort(type, property);
  }


  bindAvatarFromName(name: string): string {
    if (!name) { return '-'; }
    let imageUrl = null;
    try {
      imageUrl = require(`assets/icons/${name.split(' ').join('-').toLowerCase()}.png`);
    } catch (error) {
      imageUrl = require(`assets/icons/7-eleven.png`);
    }
    return imageUrl;
  }
}
