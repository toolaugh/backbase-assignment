import { MoneyFormat } from './money.pipe';

describe('MoneyPipe', () => {
  const moneyPipe = new MoneyFormat();

  it('Transform "5000" to be "5,000"', () => {
    expect(moneyPipe.transform('5000')).toBe('5,000');
  });

  it('Value null is not valid', () => {
    expect(moneyPipe.transform(null)).toBe('-');
  });

  it('Value undefined is not valid', () => {
    expect(moneyPipe.transform(undefined)).toBe('-');
  });

  it('Value false is not valid', () => {
    expect(moneyPipe.transform(false)).toBe('-');
  });

  it('Value null is not valid', () => {
    expect(moneyPipe.transform(true)).toBe('-');
  });

  it('Value empty is not valid', () => {
    expect(moneyPipe.transform('')).toBe('-');
  });

});
