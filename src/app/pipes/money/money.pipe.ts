import { Pipe, PipeTransform } from '@angular/core';
@Pipe({ name: 'moneyFormat' })
export class MoneyFormat implements PipeTransform {
  transform(value): string {
    if ([null, undefined, true, false, ''].includes(value)) { return '-'; }
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }
}
