import { TestBed } from '@angular/core/testing';
import { TransactionService } from './transaction.service';
import { HttpClientModule } from '@angular/common/http';
import MockTransaction from '@mock/transactions.json';
import { SortDirection, SortType } from '@constants/transaction.enum';

describe('Service: Transaction', () => {
  let transactions = null;
  let transactionService: TransactionService;
  let httpClientSpy: { get: jasmine.Spy };
  beforeEach(() => {
    // TODO: spy on other methods too
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    transactionService = new TransactionService(httpClientSpy as any);
    transactionService.transactions = MockTransaction.data;
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [TransactionService]
    });
  });

  it('Initial default transactions', () => {
    transactions = transactionService.getDefaultTransactions();
    expect(transactions).toEqual(MockTransaction.data);
  });

  it('Filter Transaction by Date', () => {
    transactions = transactionService.filterSort(SortDirection.ASC, SortType.AMOUNT);
    expect(transactions).toEqual(MockTransaction.data);
  });
});
