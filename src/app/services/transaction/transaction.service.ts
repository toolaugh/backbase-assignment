import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Transaction, TransactionResponse } from 'app/models/transaction.model';
import { map } from 'rxjs/operators';
import { SortDirection, SortType } from '@constants/transaction.enum';
import { FETCH_TRANSACTIONS } from '@constants/request';
@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private httpClient: HttpClient) { }

  transactions: Array<Transaction> = [];

  public fetchTransactions() {
    return this.httpClient.get<any>(FETCH_TRANSACTIONS)
      .pipe(
        map((response: TransactionResponse) => {
          this.transactions = JSON.parse(JSON.stringify(response)).data;
        })
      );
  }

  public getDefaultTransactions(): Array<Transaction> {
    return this.transactions;
  }

  public addNewTransaction(transaction: Transaction) {
    this.transactions.unshift(transaction);
  }

  public searchTransaction(searchText): Array<Transaction> {
    if (!searchText) {
      return this.transactions;
    }
    const filteredTransactions = this.transactions.filter(
      item => (item.merchant.name.toLocaleLowerCase().includes(searchText.toLowerCase()
        || item.transaction.type.toLowerCase().includes(searchText.toLowerCase())
        || item.transaction.amountCurrency.amount.toString().toLowerCase().includes(searchText.toLowerCase())
      ))
    );
    return filteredTransactions;
  }

  public filterSort(direction: string, property: string) {
    const sortedTransactions = this.transactions.sort(this.dynamicSort(direction, property));
    return sortedTransactions;
  }

  dynamicSort(direction: string, property: string) {
    let sortOrder = 1;
    if (direction === SortDirection.ASC) {
      sortOrder = -1;
    }
    return (a, b) => {
      switch (property) {
        case SortType.DATE:
          a[property] = new Date(a.dates.valueDate);
          b[property] = new Date(b.dates.valueDate);
          break;
        case SortType.BENEFICIARY:
          a[property] = a.merchant.name;
          b[property] = b.merchant.name;
          break;
        case SortType.AMOUNT:
          a[property] = +a.transaction.amountCurrency.amount;
          b[property] = +b.transaction.amountCurrency.amount;
          break;
      }
      const result = a[property] > b[property] ? -1 : a[property] < b[property] ? 1 : 0;
      return result * sortOrder;
    };
  }
}
